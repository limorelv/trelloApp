from flask import Flask, send_file


app = Flask(__name__, static_folder='static', static_url_path='')


@app.route('/')
def multiple_routes(**kwargs):
    return send_file('templates/index.html')


if __name__ == '__main__':
    app.debug = True
    app.run(host='127.0.0.1', debug=True)