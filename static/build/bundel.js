/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

	'use strict';

	var loadCards = function loadCards(param) {
	    var boardId = $("option:selected", '#boards').val();
	    if (boardId) {
	        Trello.get('/boards/' + boardId + '/cards', loadCardsSuccess, function () {
	            console.log("Failed to load cards");
	        });
	    }
	};

	var loadLists = function loadLists() {
	    $('#list').empty();
	    var boardId = $("option:selected", '#boards').val();
	    if (boardId) {
	        $('#add-card-div').show();
	        Trello.get('/boards/' + boardId + '/lists', loadListSuccess, function () {
	            console.log("Failed to load labels");
	        });
	    } else {
	        $('#add-card-div').hide();
	    }
	};

	var loadListSuccess = function loadListSuccess(lists) {
	    $('.list-option').remove();
	    $.each(lists, function (index, list) {
	        var listdiv = $("<div  id=" + list.id + " class=col-sm-3><h3>" + list.name + "</h3></div>");
	        $('#list').append(listdiv);
	        var lstoption = $("<option class='list-option' value=" + list.id + ">" + list.name + "</option>");
	        $('#list-select').append(lstoption);
	    });
	    loadCards();
	};

	var loadBoardsSuccess = function loadBoardsSuccess(boards) {
	    $.each(boards, function (index, value) {
	        $('#boards').append($("<option></option>").attr("value", value.id).text(value.name));
	    });
	};

	var loadBoards = function loadBoards() {
	    //Get the users boards
	    Trello.get('/members/me/boards/', loadBoardsSuccess, function () {
	        console.log("Failed to load boards");
	    });
	};

	var loadCardsSuccess = function loadCardsSuccess(cards) {
	    $.each(cards, function (index, card) {
	        var cardsp = $("<p class='card' id=" + card.id + " data-due=" + card.due + ">" + card.name + "</p>");
	        $('#' + card.idList).append(cardsp);
	    });
	};

	var filterbydate = function filterbydate() {
	    var filterdate = $('#search-date').val();
	    if (filterdate) {
	        $('.card').filter(function (index, value) {
	            return !$(value).attr('data-due').includes(filterdate);
	        }).each(function (i, v) {
	            $(v).hide();
	        });
	    }
	};

	var filterbymemmber = function filterbymemmber() {
	    var boardId = $("option:selected", '#boards').val();
	    var filtertext = $('#search-text').val();
	    if (filtertext) {
	        Trello.get('/boards/' + boardId + '/members', loadMembersSuccess, function () {
	            console.log("Failed to load members");
	        });
	    }
	};

	var addCardSuccess = function addCardSuccess(data) {
	    var card = $("<p class='card' id=" + data.idList + " data-due=" + data.due + ">" + data.name + "</p>");
	    $('#' + data.idList).append(card);
	};

	var loadMembersSuccess = function loadMembersSuccess(members) {
	    $('.card').remove();
	    //var boardId = $("option:selected", '#boards').val();
	    var filtertext = $('#search-text').val();
	    var filteredMember = $(members).filter(function (index, member) {
	        return member.fullName == filtertext;
	    })[0];

	    if (filteredMember) {
	        Trello.get('/members/' + filteredMember.id + '/cards', loadCardsSuccess(), function () {
	            console.log("Failed to load cards for member");
	        });
	    }
	};

	var filterbyname = function filterbyname() {
	    var filtertext = $('#search-text').val();
	    if (filtertext) {
	        $('.card').filter(function (index, value) {
	            return value.innerText != filtertext;
	        }).each(function (i, v) {
	            $(v).hide();
	        });
	    }
	};

	$(document).ready(function () {
	    $("#search").click(function (event) {
	        event.preventDefault();
	    });

	    $('#add-card-div').hide();

	    $("#add").click(function (event) {
	        event.preventDefault();
	    });

	    $('#boards').change(function () {
	        loadLists();
	    });

	    $('#actions').change(function () {
	        var action = $("option:selected", '#actions').val();
	        if (action == 2) {
	            $('#search-text').hide();
	            $('#search-date').show();
	        } else {
	            $('#search-text').show();
	            $('#search-date').hide();
	        }
	    });

	    $('#search').click(function () {
	        var action = $("option:selected", '#actions').val();
	        if (action == 2) {
	            filterbydate();
	            return;
	        }
	        if (action == 3) {
	            filterbymemmber();
	        }
	        if (action == 4) {
	            filterbyname();
	            return;
	        }
	    });

	    $('#add').click(function () {
	        var cardName = $('#card-name').val();
	        var listId = $("option:selected", '#list-select').val();
	        $('#card-name').val('');
	        if (cardName && listId) {
	            var newCard = { name: cardName,
	                pos: "top",
	                idList: listId
	            };

	            Trello.post('/cards/', newCard, addCardSuccess, function () {
	                console.log('NNNONOO!!!!');
	            });
	        }
	    });

	    $('#search-date').hide();

	    Trello.authorize({
	        type: "popup",
	        name: "Trello dashboard",
	        scope: {
	            read: true,
	            write: true
	        },
	        expiration: "never",
	        success: loadBoards,
	        error: function error() {
	            console.log("Failed authentication");
	        }
	    });
	});

/***/ }
/******/ ]);